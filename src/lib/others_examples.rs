fn one_line_if(){
    let ex = 5;
    let res = if ex == 5 {11} else {15};
    println!("Res: {}",res)
}

//no semicolon if u wanna return anything
fn return_type_in_function_add(x:f32,y:f32)->f32{
    x+y
}

fn tuple_example(){
    //one way
    let tup :(u32,f64,u8) = (100,245.4,1);
    let (x,y,z) = tup;
    println!("value of x,y,z from tup is:{},{},{}",x,y,z);

    //or another
    println!("value of tup from tup is:{},{},{}",tup.0,tup.1,tup.2);
}

fn array_example(){
    let arr = ["Jakub","Anna","Piotr"];
    println!("name is arr: {},{},{}",arr[0],arr[1],arr[2]);
}


#[test]
fn should_fail() {
    unimplemented!();
}