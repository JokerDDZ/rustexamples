use crate::lib::enums;

#[derive(Debug,Clone)]
pub enum IPAddrKind{
    V4(u8,u8,u8,u8),
    V6(String),
    V7{x:u32,y:u32},
    V8(u64,u64,u64),
}

pub fn better_enum_ip(){
    let home = IPAddrKind::V4(127,0,0,1);
    let loopback = IPAddrKind::V6(String::from("::1"));
    let address = IPAddrKind::V7{x:10,y:12};
    let address_v8 = IPAddrKind::V8(15,16,17);

    show_ip(&home);
    show_ip(&loopback);
    show_ip(&address);
    println!("");
    //match example
    match_ip(&address_v8);
}

pub fn show_ip(hand:&IPAddrKind){
    println!("IP: {:?}",hand);
}

fn match_ip(ip : &IPAddrKind){
    match ip {
        IPAddrKind::V4(127,0,0,1) => println!("127.0.0.1 home"),
        IPAddrKind::V4(a,b,c,d) => println!("{},{},{},{} this is V4",a,b,c,d),
        IPAddrKind::V6(s) => println!("{} This is V6",s),
        IPAddrKind::V7{x,y} => println!("x:{} , y:{} ",x,y,),
        _ => println!("I dont know what to do !"),
    }
}

pub enum Message{
    Quit,
    Move{x:i32,y:i32},
    Write(String),
    ChangeColor(i32, i32, i32),
}


impl Message{
    fn call(&self){
        println!("Message call: LOL");
    }
}

pub fn message_example(){
    let m = Message::Write(String::from("hello"));
    m.call();
}

pub enum Option<T>{
    Some(T),
    None,
}

pub fn example_of_using_options(){
    let some_number = Some(5);
    let some_string = Some("Hello world");
    let some_none: Option<u32>= enums::Option::None;
    //let absent_number:Option<i32> = None;

    match some_none {
        enums::Option::Some(n) => println!("{}",n),
        enums::Option::None => println!("No value!"),
    }
}

pub enum Coin{
    Penny,
    Nickel,
    Dime,
    Quarter,
}

pub fn value_in_cents(coin:Coin) -> u8{
    match coin{
        Coin::Penny => {println!("Lucky penny");1}
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}

pub fn example_of_coin_to_cents()
{
    println!("value in cents,nickel:{}",value_in_cents(Coin::Penny));
}
