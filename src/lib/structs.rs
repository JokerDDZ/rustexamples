pub struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

impl User {
    pub fn setup(&mut self) {
        self.set_email("prrrr");
        self.set_username("wow");
    }

    pub fn set_username(&mut self, username: &str) {}

    pub fn set_email(&mut self, email: &str) {}
}

pub fn user_test() {
    let mut user = User {
        active: true,
        username: "Jakub".to_owned(),
        email: "claud@gmail.com".to_owned(),
        sign_in_count: 5,
    };

    do_something_stupid_with_user(&mut user);
}

pub fn do_something_stupid_with_user(user: &mut User) {
    user.setup();
}

pub fn struct_example() {
    let mut user = User {
        email: String::from("abc@gmail.com"),
        username: String::from("Joker"),
        active: true,
        sign_in_count: 112,
    };

    show_info_of_user(user);
}

//contractor of struct_example
// impl struct_example(){
//     pub fn new()->struct_example{
//         struct_example{
//             active:true,
//             username:"Joker",
//             email:String::from("abc@gmail.com"),
//             sign_in_count:112,
//         }
//     }
// }

pub fn create_similar_copy_to_other_struct() {
    let user1 = User {
        email: String::from("abc@gmail.com"),
        username: String::from("Joker"),
        active: true,
        sign_in_count: 112,
    };

    let user2 = User {
        email: String::from("cba@gmail.com"),
        username: String::from("Manus"),
        ..user1
    };

    show_info_of_user(user1);
    show_info_of_user(user2);
}

fn show_info_of_user(u: User) {
    println!(
        "user data: {},{},{},{}",
        u.active, u.username, u.email, u.sign_in_count
    );
}

pub fn example_tuple_structs() {
    struct Color(f32, f32, f32);
    struct Point(f32, f32, f32);

    let white = Color(1.0, 1.0, 1.0);
    let one = Point(1.0, 1.0, 1.0);
}

//more advanced example of strctures in rust
enum Gender {
    Male,
    Female,
    NoBinary,
}

struct Person {
    name: String,
    surname: String,
    gender: Gender,
}
//*Box<> For structs
//*Box<dyn > For traits
struct School {
    headmaster: Person,
    students: Vec<Box<Person>>,
}

impl School {
    fn print_all_students(&mut self) {
        for s in &mut self.students {
            println!("name: {} ,surname:{}", s.name, s.surname);
        }
    }

    fn add_student(&mut self, new_student: Person) {
        self.students.push(Box::new(new_student));
    }

    fn get_student_by_surname(&mut self, surname: String) -> Option<&mut Box<Person>> {
        for s in &mut self.students {
            if s.surname == surname {
                return Some(s);
            }
        }

        None
    }
}

pub fn school_struct_example() {
    let headmaster = Person {
        name: "Wojtek".to_owned(),
        surname: "Kowalski".to_owned(),
        gender: Gender::NoBinary,
    };

    let student_1 = Person {
        name: "Janek".to_owned(),
        surname: "Jankowski".to_owned(),
        gender: Gender::Male,
    };

    let student_2 = Person {
        name: "Bartek".to_owned(),
        surname: "Szum".to_owned(),
        gender: Gender::Male,
    };

    let mut school = School {
        headmaster,
        students: vec![Box::new(student_1), Box::new(student_2)],
    };

    school.print_all_students();

    let student_3 = Person {
        name: "Tymek".to_owned(),
        surname: "Krakus".to_owned(),
        gender: Gender::Female,
    };

    println!("\n\nAdd student:");
    school.add_student(student_3);
    school.print_all_students();

    let student_to_find = school.get_student_by_surname(String::from("Krakus"));

    match student_to_find {
        Some(x) => println!("Student we found {}", x.name),
        None => println!("no student with this name"),
    }
}
