pub fn ownership_example(){
    let mut s = String::from("Hello");
    s.push_str(", world!"); 
    println!("{}",s);

    {
        //This is not valid: s1 is out of scopw bcs ref to it is passed to s2
        let mut s1 = String::from("Hello");
        let s2 = s1;
        //println!("S1: {}",s1);
        println!("S2: {}",s2);
    }

    {
        //clone example
        let mut s1 = String::from("Hello");
        let s2 = s1.clone();
        println!("Clone example");
        println!("S1: {}",s1);
        println!("S2: {}",s2);
    }
    
}

pub fn borrowing_example(){
    let s1 = String::from("PRRRRR");
    let l = calculate_len_of_string(&s1);
    println!("length of {} is {}",s1,l);
}

fn calculate_len_of_string(s:&String)->usize{
    s.len()
}