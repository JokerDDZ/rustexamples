

pub fn example_push_vector(){
    let v0:Vec<i32> = Vec::new();
    let mut v1 = vec![1, 2, 3];
    v1.push(4);
    v1.push(5);
    v1.push(6);

    for x in v1{
        println!("vector value: {}",x);
    }
}