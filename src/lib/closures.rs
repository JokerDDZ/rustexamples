use std::mem;

pub fn simple_closureExample() {
    let outer_var = 42;
    let closure_annotated = |i: i32| -> i32 { i + outer_var };
    let closure_inferred = |i| i + outer_var;

    println!("Closure annotated: {}", closure_annotated(1));
    println!("Closure inferred: {}", closure_inferred(1));
}

pub fn capturing() {
    let color = String::from("green");
    let print = || println!("color: {}", color);
    print();
    let _reborrow = &color;
    print();
    let _color_moved = color;

    let mut count = 0;
    let mut inc = || {
        count += 1;
        println!("Count: {}", count);
    };

    inc();
    inc();
    let _count_reborrowed = &mut count;
    let movable = Box::new(3);
    let consume = || {
        println!("Movable: {:?}", movable);
        mem::drop(movable);
    };

    consume();
}

fn apply_to_3<F>(f: F) -> i32
where
    F: Fn(i32) -> i32,
{
    f(3)
}

pub fn as_input_parameter() {
    let double = |x| 2 * x;
    println!("3 doubled: {}",apply_to_3(double));
}

fn call_me<F: Fn()>(f:F){
    f();
}

fn function(){
    println!("I'am function!");
}

pub fn input_function(){
    let closure = || println!("I'am a closure!");
    call_me(closure);
    call_me(function);
}
