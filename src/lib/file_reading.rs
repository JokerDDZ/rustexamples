use std::{fs::{self, File}, io::{BufReader, BufRead}};

fn read_file_string(filepath: &str){
    let data = fs::read_to_string(filepath).expect("Error in file reading");
    println!("Read to string:\n{}",data);
}

fn read_file_as_vector(filepath: &str){
    let data = fs::read(filepath).expect("Error in file reading");

    for n in data{
        println!("Something: {}",n);
    }
}

fn read_file_line_by_line(filepath: &str){
    let file = File::open(filepath).expect("Error in file reading");
    let reader = BufReader::new(file);

    for line in reader.lines() {
        let line = line.expect("Something is wrong in lines");
        println!("{}",line);
    }
}

pub fn print_all_examples(){
    read_file_string("./text.txt");
    read_file_as_vector("./text.txt");
    read_file_line_by_line("./text.txt");
}