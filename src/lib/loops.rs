pub fn loops_example() {
    //simple loop with break;
    let mut stop = 0;
    loop {
        println!("PRR");
        stop += 1;
        if stop >= 3 {
            break;
        }
    }

    //loop with return value - keyword:break
    let mut counter = 0;
    let record = loop {
        if counter == 10 {
            break counter;
        }
        counter += 1;
    };

    println!("Counter: {}", counter);

    //while
    let mut while_handler = 0;

    while while_handler < 10 {
        while_handler += 1;
    }

    println!("While: {}", while_handler);

    //looping through array
    let arr = [5, 6, 7, 8, 9];

    for x in arr {
        println!("value from arr: {}", x);
    }

    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}

pub fn calculate(bottom: i32, top: i32) -> i32 {
    (bottom..=top).filter(|e| e % 2 == 0).sum()
}
