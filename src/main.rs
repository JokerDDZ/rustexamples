use std::char;
use std::io::Empty;
use std::vec;

use lib::closures;
use lib::file_reading;
use lib::loops;
use lib::structs;
use rand::seq::index;
mod lib;

fn main() {
    //fileReading::print_all_examples();
    // let result = loops::calculate(2, 10);
    // println!("Result {}", result);

    // closures::simple_closureExample();
    // println!(" ");
    // closures::capturing();
    // println!(" ");
    // closures::as_input_parameter();
    // println!(" ");
    // closures::input_function();

    //two_sum(vec![3, 2, 4], 6);

    //let mut first_list_0 = Box::new(ListNode::new(2));
    //let mut first_list_1 = Box::new(ListNode::new(4));
    //let mut first_list_2 = Box::new(ListNode::new(3));
    //first_list_0.next = Some(first_list_1);
    //first_list_1.next = Some(first_list_2);

    //add_two_numbers(l1, l2)
    //let mut longest_substring = length_of_longest_substring(" ".to_string());
    //println!("longest string: {longest_substring}");
    //longest_substring = length_of_longest_substring("dvdf".to_string());
    //println!("longest string: {longest_substring}");

    let median = find_median_sorted_arrays(vec![1, 2], vec![3, 4]);
    println!("med:  {median}")
}

pub fn length_of_longest_substring(s: String) -> i32 {
    if s.is_empty() {
        return 0;
    };

    let mut vec_of_chars: Vec<char> = vec![];
    let mut longest_string_current: i32 = 0;
    let mut longest_substring: i32 = -1;

    for c in s.chars() {
        if vec_of_chars.contains(&c) {
            remove_until_char(&mut vec_of_chars, c);
            vec_of_chars.push(c);
            longest_string_current = vec_of_chars.len() as i32;
        } else {
            vec_of_chars.push(c);
            longest_string_current += 1;
        }

        if longest_string_current > longest_substring {
            longest_substring = longest_string_current;
        }
    }

    longest_substring
}

pub fn remove_until_char(characters: &mut Vec<char>, chara: char) {
    let vec_length = characters.len();

    for i in 0..vec_length {
        if characters[i] == chara {
            characters.drain(0..i + 1);
            return;
        }
    }
}

pub fn find_median_sorted_arrays(nums1: Vec<i32>, nums2: Vec<i32>) -> f64 {
    let length_1 = nums1.len();
    let length_2 = nums2.len();
    //let sum_1: f64 = nums1.iter().sum::<i32>() as f64;
    //let sum_2: f64 = nums2.iter().sum::<i32>() as f64;

    //(sum_1 + sum_2) / (length_1 + length_2) as f64
    let mut helper: Vec<i32> = vec![];
    let mut iter_1 = 0;
    let mut iter_2 = 0;
    let mut ended_1 = false;
    let mut ended_2 = false;

    loop {
        if !ended_1 && iter_1 >= length_1 {
            ended_1 = true;
        }

        if !ended_2 && iter_2 >= length_2 {
            ended_2 = true;
        }

        if ended_1 && ended_2 {
            let length_of_helper = helper.len();
            let result: f64;

            if length_of_helper % 2 == 0 {
                let index = (length_of_helper / 2) - 1;
                let sum = helper[index] + helper[index + 1];
                result = sum as f64 / 2_f64;
            } else {
                result = helper[length_of_helper / 2] as f64;
            }

            return result;
        }

        let get_number_from_1 = match ended_1 {
            true => i32::MAX,
            false => nums1[iter_1],
        };

        let get_number_from_2 = match ended_2 {
            true => i32::MAX,
            false => nums2[iter_2],
        };

        if get_number_from_1 <= get_number_from_2 {
            helper.push(get_number_from_1);
            iter_1 += 1;
        } else {
            helper.push(get_number_from_2);
            iter_2 += 1;
        }
    }
}

pub fn get_common(nums1: Vec<i32>, nums2: Vec<i32>) -> i32 {
    if nums1[nums1.len() - 1] < nums2[0] {
        return -1;
    }

    for x in nums1 {
        if nums2.contains(&x) {
            return x;
        }
    }

    -1
}

pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
    let length = nums.len();

    for i in 0..length {
        for j in (i + 1)..length {
            if nums[i] + nums[j] == target {
                return vec![i as i32, j as i32];
            }
        }
    }

    vec![0, 0]
}

// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

//pub fn add_two_numbers(
//    l1: Option<Box<ListNode>>,
//    l2: Option<Box<ListNode>>,
//) -> Option<Box<ListNode>> {
//let l1 = l1.unwrap();
//let l2 = l2.unwrap();
//let first_node: Box<ListNode> = Box::new(ListNode::new(0));
//let mut curr_node = first_node.clone();
//let mut add_to_next_node = false;

// loop {
//     if l1.next.is_none() {
//         break;
//     }

//     let mut sum = l1.val + l2.val;

//     if add_to_next_node {
//         sum += 1;
//     }

//     if sum >= 10 {
//         add_to_next_node = true;
//         sum -= 10;
//     } else {
//         add_to_next_node = false;
//     }

//     curr_node.val = sum;
//     curr_node.next = Some(Box::new(ListNode::new(0)));
// }

//Some(first_node)

// match (l1, l2) {
//     (None, None) => None,
//     (Some(n), None) | (None, Some(n)) => Some(n),
//     (Some(n1), Some(n2)) => {
//         let sum = n1.val + n2.val;

//         if sum < 10 {
//             Some(Box::new(ListNode {
//                 val: sum,
//                 next: add_two_numbers(n1.next, n2.next),
//             }))
//         } else {
//             let carry = Some(Box::new(ListNode::new(1)));
//             Some(Box::new(ListNode {
//                 val: sum - 10,
//                 next: add_two_numbers(add_two_numbers(carry, n1.next), n2.next),
//             }))
//         }
//     }
// }
//}
